console.log(`>>ES6/ECMAScript2015 UPDATES ACTIVITY<<`)
// 4-----------------------------
 const getCube= Math.pow(8,3);  
 console.log(`The cude of 8 is ${getCube}`);

 // 5-6 -----------------------------
const fullAddress = ["258 Washington Ave NW", "California 90011"];
const[street ,city ]=fullAddress;
console.log(`I live at ${street}, ${city}.`)

 // 7-8 -----------------------------
 const animal = {
	name: `Lolong`,
	weight: `1075 kg`,
	measurement: `10 tf 3 in`
}
const {name , weight, measurement} =animal;

function animalDescription({name , weight, measurement}){
	console.log(`${name} was a saltwater crocodile. He weight at ${weight} with a measurement of  ${measurement}.`)
}

animalDescription(animal);

 // 9-10 -----------------------------


class dog{
	constructor(name , age , breed){
		this.name = name;
		this.age = age;
		this.breed = breed;
	}
}

const mydog= new dog (`Frankie`, `5`, `Miniature Dachshund`);
console.log(mydog);